defmodule Exsms do
  use HTTPoison.Base
  require Logger

  def send(service, payload) do
    case check_service(service) do
      true ->
        process_response_body(service, payload)

      false ->
        {:error, "Service Not Found"}
    end
  end

  def process_response_body(service, body) do
    config = serviceconfig(service)
    headers = auth_headers(service, config)

    phone_number = build_phone_number(service, body[:country], body[:to])

    pre_payload = build_payload(config, service, body, phone_number)
    payload = pre_payload |> Poison.encode!()
    url = build_url(config, service, pre_payload)

    request(config[:type], url, payload, headers)
  end

  def build_url(config, service, params) do
    case service do
      service when service in [:textlocal, :msg91_otp] ->
        config[:url] <> URI.encode_query(params)

      _ ->
        config[:url]
    end
  end

  def build_payload(config, service, payload, phone_number) do
    case service do
      :textlocal ->
        %{
          apikey: config[:api],
          numbers: phone_number,
          message: payload[:message],
          sender: payload[:sender]
        }

      :mailjet ->
        %{
          To: phone_number,
          Text: payload[:message],
          From: payload[:sender]
        }

      :sendinblue ->
        %{
          recipient: phone_number,
          content: payload[:message],
          sender: payload[:sender],
          type: payload[:type],
          tag: payload[:tag]
        }

      :msg91 ->
        {:ok, phone_number} = ExPhoneNumber.parse(phone_number, payload[:country])

        %{
          sender: payload[:sender],
          route: config[:route],
          country: phone_number.country_code,
          sms: [
            %{
              message: payload[:message],
              to: [
                Integer.to_string(phone_number.national_number)
              ]
            }
          ]
        }

      :msg91_otp ->
        %{
          authkey: config[:api],
          sender: payload[:sender],
          mobile: phone_number,
          message: payload[:message],
          otp_length: payload[:length],
          otp: payload[:otp],
          otp_expiry: payload[:otp_expiry],
          email: payload[:email]
        }
    end
  end

  def auth_headers(service, config) do
    case service do
      :sendinblue ->
        [{"api-key", config[:api]}, {"Content-Type", "application/json"}]

      :mailjet ->
        [{"Authorization", "Bearer " <> config[:api]}, {"Content-Type", "application/json"}]

      :msg91 ->
        [{"authkey", config[:api]}, {"Content-Type", "application/json"}]

      _ ->
        []
    end
  end

  def build_phone_number(service, country, phone) do
    {:ok, phone_number} = ExPhoneNumber.parse(phone, country)

    case service do
      n when n in [:sendinblue, :textlocal, :msg91_otp] ->
        "#{phone_number.country_code}#{phone_number.national_number}"

      :msg91 ->
        "#{phone_number.national_number}"

      :mailjet ->
        ExPhoneNumber.format(phone_number, :e164)
    end
  end

  def serviceconfig(service) do
    case service do
      :sendinblue ->
        %{
          url: "https://api.sendinblue.com/v3/transactionalSMS/sms",
          type: :post,
          api: Application.get_env(:exsms, :sendinblue)[:api]
        }

      :mailjet ->
        %{
          url: "https://api.mailjet.com/v4/sms-send",
          type: :post,
          api: Application.get_env(:exsms, :mailjet)[:api]
        }

      :textlocal ->
        %{
          url: "https://api.textlocal.in/send/?",
          type: :post,
          api: Application.get_env(:exsms, :textlocal)[:api]
        }

      :msg91 ->
        %{
          url: "https://api.msg91.com/api/v2/sendsms",
          type: :post,
          api: Application.get_env(:exsms, :msg91)[:api],
          route: Application.get_env(:exsms, :msg91)[:route]
        }

      :msg91_otp ->
        %{
          url: "https://control.msg91.com/api/sendotp.php?",
          type: :post,
          api: Application.get_env(:exsms, :msg91)[:api]
        }

      _ ->
        "Service Not Found"
    end
  end

  defp check_service(service) do
    case service do
      service when service in [:sendinblue, :mailjet, :textlocal, :msg91, :msg91_otp] ->
        true

      _ ->
        false
    end
  end

  def request(method, url, body, headers) do
    url = String.to_charlist(url)

    Logger.debug("exsms - Method - #{inspect(method)}")
    Logger.debug("exsms - Url - #{inspect(url)}")
    Logger.debug("exsms - Body - #{inspect(body)}")
    Logger.debug("exsms - Headers - #{inspect(headers)}")

    HTTPoison.request(method, url, body, headers)
    |> normalise_response
  end

  defp normalise_response(response) do
    Logger.debug("exsms - #{inspect(response)}")

    case response do
      {:ok, %HTTPoison.Response{body: body}} ->
        {:ok, body}

      {:error, %HTTPoison.Error{reason: reason}} ->
        {:error, reason}
    end
  end
end
