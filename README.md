# Exsms [![pipeline status](https://gitlab.com/ahamtech/elixir/exsms/badges/master/pipeline.svg)](https://gitlab.com/ahamtech/elixir/exsms/commits/master)

"Exsms: A Elixir library for sending transactional SMS - supoprts Sendinblue, mailjet ,msg91 and textlocal"

## Supported Services

* textlocal (India)
* Sendinblue
* Mailjet
* Msg91

MR is always Supported

## Installation
```elixir
def deps do
  [
    {:exsms, "~> 0.2.1"}
  ]
end
```

### Usage

#### Textlocal

```elixir
payload =  %{
  to: "9000123456",
  message: "test message",
  sender: "EXSMS",
  country: "IN"
}
Exsms.send(:textlocal, payload)
```

#### Sendinblue

```elixir
payload =  %{
  to: "9000123456",
  message: "test message",
  sender: "EXSMS",
  country: "IN"
}
Exsms.send(:sendinblue, payload)
```

#### Mailjet

```elixir
payload =  %{
  to: "9000123456",
  message: "test message",
  sender: "EXSMS",
  country: "IN"
}
Exsms.send(:mailjet, payload)
```

#### Msg91

```elixir
payload =  %{
  to: "9000123456",
  message: "test message",
  sender: "EXSMS",
  country: "IN",
  route: 4
}
Exsms.send(:msg91, payload)
```

#### Msg91_otp

```elixir
%{                                   
  email: "help@test.in",
  message: "Verification OTP 947751",
  otp: 947751,
  sender: "AHAMTE",
  to: "9000123456",
  country: "IN"
}
Exsms.send(:msg91_otp, payload)
```

#### Config

```elixir
config :exsms, :sendinblue, api: "xkeysib"

config :exsms, :mailjet, api: "123"

config :exsms, :textlocal, api: "123"

config :exsms, :msg91, api: "123"

config :exsms, :msg91_otp, api: "123"
```


## License
Exsms is Copyright © Ahamtech. It is free software, and may be redistributed under the terms specified in the LICENSE file.

## About Ahamtech

![img](https://sos-ch-dk-2.exo.io/aham-web/company/ahamtech.png)

> Exsms is maintained by Ahamtech.

We love open source software, Erlang, Elixir, and Phoenix. See our other opensource projects, or hire our Elixir Phoenix development team to design, develop, and grow your product.
