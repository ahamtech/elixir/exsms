defmodule ExsmsTest do
  use ExUnit.Case
  alias Exsms

  test "Config Non proivder" do
    service = :no_provider
    config = Exsms.serviceconfig(service)
    assert config == "Service Not Found"
  end

  test "Config Textlocal" do
    service = :textlocal
    config = Exsms.serviceconfig(service)
    assert config.url == "https://api.textlocal.in/send/?"
    assert config.type == :post
  end

  test "Config mailjet" do
    service = :mailjet
    config = Exsms.serviceconfig(service)
    assert config.url == "https://api.mailjet.com/v4/sms-send"
    assert config.type == :post
  end

  test "Config sendinblue" do
    service = :sendinblue
    config = Exsms.serviceconfig(service)
    assert config.url == "https://api.sendinblue.com/v3/transactionalSMS/sms"
    assert config.type == :post
  end

  test "Config msg91" do
    service = :msg91
    config = Exsms.serviceconfig(service)
    assert config.url == "https://api.msg91.com/api/v2/sendsms"
    assert config.type == :post
  end

  test "Auth headers textlocal" do
    service = :textlocal
    config = %{api: "123456"}
    header = Exsms.auth_headers(service, config)
    assert header == []
  end

  test "Build payload mailjet" do
    service = :mailjet
    config = %{api: "123456", to: "123", message: "text", sender: "sender"}
    payload = Exsms.build_payload(service, service, config, config[:to])

    assert payload[:From] == config.sender
    assert payload[:To] == config.to
    assert payload[:Text] == config.message
  end

  test "Build payload sendinblue" do
    service = :sendinblue
    config = %{api: "123456", to: "123", message: "text", sender: "sender"}
    payload = Exsms.build_payload(service, service, config, config[:to])

    assert payload[:sender] == config.sender
    assert payload[:recipient] == config.to
    assert payload[:content] == config.message
  end

  test "Build payload textlocal" do
    service = :textlocal
    config = %{to: "123", message: "text", sender: "sender"}
    payload = Exsms.build_payload(config, service, config, config[:to])

    assert payload[:sender] == config.sender
    assert payload[:numbers] == config.to
    assert payload[:message] == config.message
  end

  test "Build payload msg91" do
    service = :msg91

    config = %{
      api: "123456",
      to: "456123",
      message: "text",
      sender: "sender",
      route: 4,
      country: "IN"
    }

    payload = Exsms.build_payload(config, service, config, config[:to])

    assert payload[:route] == config.route
    assert payload[:sender] == config.sender
    assert payload[:sms] == [%{message: config.message, to: [config.to]}]
  end

  test "Build payload msg91_otp" do
    service = :msg91_otp
    config = %{to: "123", message: "text", sender: "sender", otp: 123, country: "IN"}
    payload = Exsms.build_payload(config, service, config, config[:to])

    assert payload[:otp] == config.otp
    assert payload[:sender] == config.sender
  end
end
